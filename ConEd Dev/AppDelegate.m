//
//  AppDelegate.m
//  ConEd Dev
//
//  Created by Hedgehog on 02.07.14.
//  Copyright (c) 2014 Mobilez365. All rights reserved.
//

#import "AppDelegate.h"
#import "WebViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[[WebViewController alloc] init]];
    navigationController.navigationBarHidden = YES;
    self.window.rootViewController = navigationController;
    return YES;
}
@end
