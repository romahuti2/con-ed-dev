//
//  WebViewController.m
//  ConEd
//
//  Created by Hedgehog on 01.07.14.
//  Copyright (c) 2014 Mobilez365. All rights reserved.
//

#import "WebViewController.h"

static NSString *MainUrl = @"http://staging2.platform.honestbuildings.com/";

@interface WebViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

- (IBAction)loadFromUrl:(id)sender;

@end

@implementation WebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.webView.delegate = self;
    NSURL *url = [NSURL URLWithString:MainUrl];
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self performSelector:@selector(hide) withObject:nil afterDelay:0.1];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if ([self.webView.request.URL.absoluteString isEqualToString:MainUrl]) {
        [self.webView stringByEvaluatingJavaScriptFromString:@"var script = document.createElement('script');"
         "script.type = 'text/javascript';"
         "script.text = \"function makeItBetter() { "
         "var header = document.getElementById('header');"
         "header.setAttribute('style', 'padding: 0px;');"
         "document.getElementsByClassName('alignright')[0].parentElement.removeChild(document.getElementsByClassName('alignright')[0]);"
         "var head = document.getElementsByClassName('header-social')[0];"
         "head.setAttribute('style', 'min-height:0px;');"
         "var par = document.getElementsByTagName('footer')[0];"
         "par.parentNode.insertBefore(head, par);"
         "par.parentElement.removeChild(par);"
         "var div = document.createElement('div');"
         "div.setAttribute('class', 'logo');"
         "div.setAttribute('style', 'float:right');"
         "var but1 = document.createElement('a');"
         "but1.setAttribute('href', 'http://staging2.wpm.dev.honestbuildings.com/mp-application');"
         "but1.setAttribute('target', '_self');"
         "but1.setAttribute('style', 'margin-right:0px;margin-top:30px;margin-left:20px;margin-bottom:30px;');"
         "but1.setAttribute('class', 'continue button large default');"
         "but1.innerHTML = 'Become a Market Partner';"
         "var but2 = document.createElement('a');"
         "but2.setAttribute('href', 'http://staging2.wpm.dev.honestbuildings.com/ee-program-application');"
         "but2.setAttribute('target', '_self');"
         "but2.setAttribute('style', 'margin-right:0px;margin-top:30px;margin-left:20px;margin-bottom:30px;');"
         "but2.setAttribute('class', 'continue button large default');"
         "but2.innerHTML = 'Apply for a Rebate';"
         "div.appendChild(but2);"
         "div.appendChild(but1);"
         "document.getElementById('header-banner').parentElement.appendChild(div);"
         "document.getElementById('header-banner').parentElement.removeChild(document.getElementById('header-banner'));"
         "}\";"
         "document.getElementsByTagName('head')[0].appendChild(script);"];
        [webView stringByEvaluatingJavaScriptFromString:@"makeItBetter();"];
    } else if ([self.webView.request.URL.absoluteString hasPrefix:MainUrl]) {
        [self.webView stringByEvaluatingJavaScriptFromString:@"var script = document.createElement('script');"
         "script.type = 'text/javascript';"
         "script.text = \"function deleteShit() { "
         "document.getElementsByClassName('reading-box-container')[0].parentElement.parentElement.parentElement.removeChild(document.getElementsByClassName('reading-box-container')[0].parentElement.parentElement);"
         "}\";"
         "document.getElementsByTagName('head')[0].appendChild(script);"];
        [webView stringByEvaluatingJavaScriptFromString:@"deleteShit();"];
    }
    [self performSelector:@selector(show) withObject:nil afterDelay:0.2];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self performSelector:@selector(show) withObject:nil afterDelay:0.1];
}

#pragma mark - Actions

- (IBAction)loadFromUrl:(id)sender
{
    if (![self.textField.text isEqualToString:@""]) {
        [self.view endEditing:YES];
        NSURL *url = [NSURL URLWithString:self.textField.text];
        [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
    }
}

- (void)show
{
    [self.activityIndicator stopAnimating];
    self.activityIndicator.hidden = YES;
    self.webView.hidden = NO;
}

- (void)hide
{
    [self.activityIndicator startAnimating];
    self.activityIndicator.hidden = NO;
    self.webView.hidden = YES;
}

@end