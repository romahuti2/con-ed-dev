//
//  WebViewController.h
//  ConEd
//
//  Created by Hedgehog on 01.07.14.
//  Copyright (c) 2014 Mobilez365. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController<UIWebViewDelegate>

@end
